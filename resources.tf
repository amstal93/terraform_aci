/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:
 
      Each resource type is implemented by a provider. Resource blocks describe the infrastructure objects.
      It is important to note, that ACI contains thousands of managed objects in a Management Information Tree,
      but only the resources defined in this file will be referenced.

      Resources can be imported from an existing APIC, by first defining a 'stub' resource (a resource definition
      with no attributes) and issuing the `terraform import <resource> <Dn>` 
      (e.g. `terraform import aci_tenant.FLINT uni/tn-FLINT`). Following that command, issue the `terraform state` 
      command to view the retrieved state, and update the stub resource.
      
      This configuration defines tenants and VRFs associated with one of the tenants.

    references:
      - https://www.terraform.io/docs/configuration/resources.html
      - https://registry.terraform.io/providers/CiscoDevNet/aci/latest/docs/resources/fvtenant
      - https://registry.terraform.io/providers/CiscoDevNet/aci/latest/docs/resources/fvctx

*/
#
#  Tenants - specify two ACI Tenants 
#
resource "aci_tenant" "BROWNFOX" {
  name        = "BROWNFOX"
  description = "${local.service_request} by ${local.initiator}"
  annotation  = "FOX"
}

resource "aci_tenant" "FLINT" {
  name                   =  "FLINT"
  description            =  "${local.service_request} by ${local.initiator}"
  name_alias             =  "GWP"
}

#
#  VRFs - Note the Dn of the Tenant must be provided, which is exported as `id` from the `aci_tenant` resource.
#
resource "aci_vrf" "GREEN" {
  tenant_dn              = aci_tenant.BROWNFOX.id
  name                   = "GREEN"
  description            = "${local.service_request} by ${local.initiator}"
  annotation             = "GREEN_vrf"
  bd_enforced_enable     = "no"
  ip_data_plane_learning = "enabled"
  knw_mcast_act          = "permit"
  name_alias             = "alias_vrf"
  pc_enf_dir             = "egress"
  pc_enf_pref            = "unenforced"
}

resource "aci_vrf" "dmzvrf" {
  tenant_dn              = aci_tenant.BROWNFOX.id
  name                   = "DMZ_vrf"
  annotation             = "tag_vrf"
  bd_enforced_enable     = "no"
  ip_data_plane_learning = "enabled"
  knw_mcast_act          = "permit"
  name_alias             = "alias_vrf"
  pc_enf_dir             = "egress"
  pc_enf_pref            = "unenforced"
}

resource "aci_vrf" "insidevrf" {
  tenant_dn              = aci_tenant.BROWNFOX.id
  name                   = "inside_vrf"
  annotation             = "tag_vrf"
  bd_enforced_enable     = "no"
  ip_data_plane_learning = "enabled"
  knw_mcast_act          = "permit"
  name_alias             = "alias_vrf"
  pc_enf_dir             = "egress"
  pc_enf_pref            = "unenforced"
}

#
#  use CSV input
#
resource "aci_bridge_domain" "bd_for_subnet" {
  for_each    = { for bd in local.vlans : bd.vlan_id => bd }
  tenant_dn   = aci_tenant.BROWNFOX.id
  name        = each.value.vlan_name
  description = "${local.service_request} by ${local.initiator}"
}
