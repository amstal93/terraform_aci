/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      A local value assigns a name to an expression, so you can use it multiple times within a module without repeating it.

      In this example, two variables are defined, to identify the engineer initiating the workflow and a reference to the 
      service request ticket.

      Additionally, the function 'csvdecode' illustrates including the rows from a CSV file and assigning the values of the rows
      to a list of maps data structure for processing.

    references:
      - https://www.terraform.io/docs/configuration/locals.html
      - https://www.terraform.io/docs/language/functions/csvdecode.html
      - Thanks to Wasantha Hewawal Gamage for contributing to this example

*/

locals {
  initiator = "Joel W. King @joelwking"
  service_request = "CHG0123456"

  vlans = csvdecode(file("csv/vlans.csv"))

  csv_inline = <<-CSV
    vlan_id,vlan_name
    101,VLAN101
    102,VLAN102
  CSV

  inline_vlans = csvdecode(local.csv_inline)
}