/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:
 
      To declare variables use the variable block in one of your .tf files, such as `variables.tf`.
      A .tfvars file is used to assign values to variables that have already been declared in .tf files.

      Input variables are used as arguments for a Terraform module. If you are familiar with Python data types, 
      the Terraform varaible block allows you to specify the data type of the variable, e.g. string, number, set, etc.

      However, the default values can declaired and used in the variable block. An example of this is
      the variable `apic_url`. The username and password of the APIC are specifed as environment variables
      and in `credentials.auto.tfvars`.

      In this file, we define varibles for the main module, and the sub-module, `lldp`.

    references:
      - https://www.terraform.io/docs/configuration/variables.html

*/
 
#
#  Main module definitions
#
variable "apic_username" {
  description = "basic authentication for APIC"
  default = "admin"
  type = string
}

variable "apic_url" {
  description = "URL of the APIC"
  default = "https://sandboxapicdc.cisco.com/"
  type = string
}

variable "apic_password" {
  description = "basic authentication for APIC"
  default = "CHANGEME"
  type = string
}
#
# Sub-module definitions
#
variable lldp {
  description = "LLDP Policies"
  type        = map
  default     = {
    primary = {
        name        = "lldp_primary",
        description = "",
        admin_rx_st = "disabled",
        admin_tx_st = "enabled"
    },
    secondary = {
        name        = "lldp_secondary",
        description = "",
        admin_rx_st = "enabled",
        admin_tx_st = "enabled"
    }
  }
}
