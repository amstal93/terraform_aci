/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      Terraform settings are gathered together into terraform blocks.
 
    references:
      - https://www.terraform.io/docs/configuration/terraform.html

*/                  

terraform {
  required_providers {
    aci = {
      source = "ciscodevnet/aci"
      version = "~> 0.4.1"
    }
  }
  required_version = ">= 0.13"
}
