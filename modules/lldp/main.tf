/*
    Copyright (c) 2020 World Wide Technology, LLC
    All rights reserved.
    
    author: Joel W. King  @joelwking

    usage:

      This is a sub-module for the Terraform ACI demonstration, which manages the LLDP Interface policy

    references:
      - https://www.terraform.io/docs/configuration/modules.html#multiple-instances-of-a-module
      - https://www.terraform.io/docs/modules/index.html#standard-module-structure

*/
#
#  LLDP Policy
#
resource "aci_lldp_interface_policy" "lldp_policy" {

    description = var.lldp_description
    name        = var.lldp_name
    admin_rx_st = var.lldp_admin_rx_st               # Optional
    admin_tx_st = var.lldp_admin_tx_st               # Optional

    annotation  = var.lldp_annotation                # Optional
    name_alias  = var.lldp_name_alias                # Optional
}